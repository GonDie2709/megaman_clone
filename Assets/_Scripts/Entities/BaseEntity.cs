﻿using GonDie.Utils.Extensions;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BaseEntity : MonoBehaviour
{
    #region Fields

    [SerializeField] protected float _moveSpeed = 2f;
    [SerializeField] protected float _jumpStrenght = 0.5f;
    [SerializeField] protected bool _hasGravity = true;

    protected Vector2 _speed;
    protected Vector2 _moveDirection;

    protected bool _isRunning;
    protected bool _isJumping;
    protected bool _canJump;
    protected bool _canDoubleJump;
    protected bool _isGrounded;
    protected bool _isFalling;

    protected bool _blockRight, _blockLeft, _blockTop;
    protected Enums.Collisions _collisions;

    protected Transform _transform;
    protected Collider2D _collider;
    protected Animator _animator;

    private List<RaycastHit2D> _hitBottom;
    private List<RaycastHit2D> _hitTop;
    private List<RaycastHit2D> _hitLeft;
    private List<RaycastHit2D> _hitRight;

    private float _limitMaxX, _limitMinX, _limitMaxY, _limitMinY;
    private float _jumpTimeThreshold, _doubleJumpTimeThreshold;
    protected int _jumps;

    #endregion

    #region Properties

    public Vector2 Speed { get { return _speed; } }
    public bool IsRunning { get { return _isRunning; } protected set { _isRunning = value; _animator.SetBool("isRunning", value); } }
    public bool IsJumping { get { return _isJumping; } protected set { _isJumping = value; _animator.SetBool("isJumping", value); } }
    public bool IsGrounded { get { return _isGrounded; } protected set { _isGrounded = value; _animator.SetBool("isGrounded", value); if (value) IsFalling = false; } }
    public bool IsFalling { get { return _isFalling; } protected set { _isFalling = value; _animator.SetBool("isFalling", value); } }

    public Enums.Collisions Collisions { get { return _collisions; } }

    #endregion

    void Awake ()
    {
        _transform = GetComponent<Transform>();
        _collider = GetComponent<Collider2D>();
        _animator = GetComponent<Animator>();
    }

    protected virtual void Update ()
    {
        #region Set Limits

        _limitMinY = CheckLimitBottom();

        if(_speed.y > 0)
            _limitMaxY = CheckLimitTop();

        if (_speed.x > 0)
            _limitMaxX = CheckLimitRight();

        if (_speed.x < 0)
            _limitMinX = CheckLimitLeft();

        #endregion

        #region Handle Collisions

        IsGrounded = _collider.GetBound(Enums.ColliderBound.Bottom).y <= _limitMinY + 0.1f;
        _blockTop = _collider.GetBound(Enums.ColliderBound.Top).y >= _limitMaxY - 0.1f;
        _blockRight = _collider.GetBound(Enums.ColliderBound.Right).x >= _limitMaxX - 0.1f;
        _blockLeft = _collider.GetBound(Enums.ColliderBound.Left).x <= _limitMinX + 0.1f;

        _collisions = (_isGrounded ? Enums.Collisions.Bottom : Enums.Collisions.None) | (_blockTop ? Enums.Collisions.Top : Enums.Collisions.None) | 
                        (_blockRight ? Enums.Collisions.Right : Enums.Collisions.None) | (_blockLeft ? Enums.Collisions.Left: Enums.Collisions.None);

        if (_isGrounded && !IsJumping)
        {
            _speed.y = 0f;
            _canJump = true;
            _canDoubleJump = true;
            _jumps = 0;
            _jumpTimeThreshold = 0f;
            _doubleJumpTimeThreshold = 0f;
        }
        else if (_hasGravity && !_isGrounded)
        {
            _speed += GameData.Instance.gravity * Time.deltaTime;
            _speed.y = Mathf.Clamp(_speed.y, -GameData.Instance.maxAcelSpeed.y, GameData.Instance.maxAcelSpeed.y);
        }

        if (!IsRunning)
        {
            _speed.x = 0f;
        }
        else
        {
            _transform.localScale = new Vector3(Mathf.Sign(_speed.x), 1f, 1f);

            if ((_speed.x > 0f && _blockRight) || (_speed.x < 0f && _blockLeft))
                _speed.x = 0f;

            _speed.x = _speed.x * (_isGrounded ? GameData.Instance.groundFriction : GameData.Instance.airFriction);
        }

        if (IsJumping)
        {
            if (_canJump)
            {
                _jumpTimeThreshold += Time.deltaTime;

                if (_jumpTimeThreshold >= GameData.Instance.limitJumpTime)
                    _canJump = false;
            }
            else if (_canDoubleJump)
            {
                _doubleJumpTimeThreshold += Time.deltaTime;

                if (_doubleJumpTimeThreshold >= GameData.Instance.limitJumpTime)
                    _canDoubleJump = false;
            }

            if (_blockTop)
                _speed.y = 0f;

            if (_speed.y <= 0f)
                IsJumping = false;
        }
        else if (_speed.y < 0f)
            IsFalling = true;

        #endregion

        Vector3 pos = _transform.localPosition;

        pos.y = Mathf.Clamp(pos.y + _speed.y * Time.deltaTime, _limitMinY, _limitMaxY);
        if (IsGrounded && pos.y - _collider.bounds.extents.y < _limitMinY)
            pos.y = _limitMinY + _collider.bounds.extents.y + 0.01f;

        pos.x = Mathf.Clamp(pos.x + _speed.x * Time.deltaTime, _limitMinX, _limitMaxX);
        if (pos.x + _collider.bounds.extents.x > _limitMaxX)
            pos.x = _limitMaxX - _collider.bounds.extents.x - 0.01f;

        if (pos.x - _collider.bounds.extents.x < _limitMinX)
            pos.x = _limitMinX + _collider.bounds.extents.x + 0.01f;

        _transform.localPosition = pos;

        IsRunning = false;
	}
    
    private float CheckLimitBottom()
    {
        float passes = 2f;
        bool collided = false;
        _hitBottom = new List<RaycastHit2D>();

        for(int i = 0; i < passes; i++)
        {
            _hitBottom.Add(Physics2D.Raycast(Vector2.Lerp(_collider.GetBound(Enums.ColliderBound.Left), _collider.GetBound(Enums.ColliderBound.Right), i / (passes - 1)),
                                              Vector2.down, GameData.Instance.physicsMaxRayDistance, ~LayerMasks.Player));

            if (GameData.Instance.debug)
                Debug.DrawLine(Vector2.Lerp(_collider.GetBound(Enums.ColliderBound.Left), _collider.GetBound(Enums.ColliderBound.Right), i / (passes - 1)),
                    _hitBottom[i].collider != null ? _hitBottom[i].point : Vector2.Lerp(_collider.GetBound(Enums.ColliderBound.Left), _collider.GetBound(Enums.ColliderBound.Right), i / (passes - 1)) + Vector2.down * GameData.Instance.physicsMaxRayDistance,
                    Color.blue);
        }

        _hitBottom = _hitBottom.FindAll(x => x.collider != null);
        collided = _hitBottom.Any(x => x.collider != null);

        return collided ? _hitBottom.OrderBy(x => x.distance).FirstOrDefault().collider.GetBound(Enums.ColliderBound.Top).y : 
                          (_collider.GetBound(Enums.ColliderBound.Bottom) + Vector2.down * GameData.Instance.physicsMaxRayDistance).y;
    }

    private float CheckLimitTop()
    {
        float passes = 2f;
        bool collided = false;
        _hitTop = new List<RaycastHit2D>();

        for (int i = 0; i < passes; i++)
        {
            _hitTop.Add(Physics2D.Raycast(Vector2.Lerp(_collider.GetBound(Enums.ColliderBound.Left), _collider.GetBound(Enums.ColliderBound.Right), i / (passes - 1)),
                                              Vector2.up, GameData.Instance.physicsMaxRayDistance, ~LayerMasks.Player));

            if (GameData.Instance.debug)
                Debug.DrawLine(Vector2.Lerp(_collider.GetBound(Enums.ColliderBound.Left), _collider.GetBound(Enums.ColliderBound.Right), i / (passes - 1)),
                    _hitTop[i].collider != null ? _hitTop[i].point : Vector2.Lerp(_collider.GetBound(Enums.ColliderBound.Left), _collider.GetBound(Enums.ColliderBound.Right), i / (passes - 1)) + Vector2.up * GameData.Instance.physicsMaxRayDistance,
                    Color.blue);
        }

        _hitTop = _hitTop.FindAll(x => x.collider != null);
        collided = _hitTop.Any(x => x.collider != null);

        return collided ? _hitTop.OrderBy(x => x.distance).FirstOrDefault().collider.GetBound(Enums.ColliderBound.Bottom).y : 
                          (_collider.GetBound(Enums.ColliderBound.Top) + Vector2.up * GameData.Instance.physicsMaxRayDistance).y;
    }

    private float CheckLimitRight()
    {
        float passes = 6f;
        bool collided = false;
        _hitRight = new List<RaycastHit2D>();

        for (int i = 0; i < passes; i++)
        {
            _hitRight.Add(Physics2D.Raycast(Vector2.Lerp(_collider.GetBound(Enums.ColliderBound.Top), _collider.GetBound(Enums.ColliderBound.Bottom) + Vector2.up * 0.01f, i / (passes - 1)),
                                              Vector2.right, GameData.Instance.physicsMaxRayDistance, ~LayerMasks.Player));

            if (GameData.Instance.debug)
                Debug.DrawLine(Vector2.Lerp(_collider.GetBound(Enums.ColliderBound.Top), _collider.GetBound(Enums.ColliderBound.Bottom) + Vector2.up * 0.01f, i / (passes - 1)), 
                    _hitRight[i].collider != null ? _hitRight[i].point : Vector2.Lerp(_collider.GetBound(Enums.ColliderBound.Top), _collider.GetBound(Enums.ColliderBound.Bottom), i / (passes - 1)) + Vector2.right * GameData.Instance.physicsMaxRayDistance, 
                    Color.green);
        }

        _hitRight = _hitRight.FindAll(x => x.collider != null);
        collided = _hitRight.Any(x => x.collider != null);

        return collided ? _hitRight.OrderBy(x => x.distance).FirstOrDefault().collider.GetBound(Enums.ColliderBound.Left).x : 
                          (_collider.GetBound(Enums.ColliderBound.Right) + Vector2.right * GameData.Instance.physicsMaxRayDistance).x;
    }

    private float CheckLimitLeft()
    {
        float passes = 6f;
        bool collided = false;
        _hitLeft = new List<RaycastHit2D>();

        for (int i = 0; i < passes; i++)
        {
            _hitLeft.Add(Physics2D.Raycast(Vector2.Lerp(_collider.GetBound(Enums.ColliderBound.Top), _collider.GetBound(Enums.ColliderBound.Bottom), i / (passes - 1)),
                                              Vector2.left, GameData.Instance.physicsMaxRayDistance, ~LayerMasks.Player));

            if (GameData.Instance.debug)
                Debug.DrawLine(Vector2.Lerp(_collider.GetBound(Enums.ColliderBound.Top), _collider.GetBound(Enums.ColliderBound.Bottom), i / (passes - 1)),
                    _hitLeft[i].collider != null ? _hitLeft[i].point : Vector2.Lerp(_collider.GetBound(Enums.ColliderBound.Top), _collider.GetBound(Enums.ColliderBound.Bottom), i / (passes - 1)) + Vector2.left * GameData.Instance.physicsMaxRayDistance,
                    Color.green);
        }

        _hitLeft = _hitLeft.FindAll(x => x.collider != null);
        collided = _hitLeft.Any(x => x.collider != null);

        return collided ? _hitLeft.OrderBy(x => x.distance).FirstOrDefault().collider.GetBound(Enums.ColliderBound.Right).x :
                          (_collider.GetBound(Enums.ColliderBound.Left) + Vector2.left * GameData.Instance.physicsMaxRayDistance).x;
    }

    protected virtual void Move(Vector2 direction)
    {
        IsRunning = true;

        direction.Normalize();
        _speed.x = Mathf.Clamp(direction.x * _moveSpeed, -GameData.Instance.maxAcelSpeed.x, GameData.Instance.maxAcelSpeed.x);
    }

    protected virtual void Jump(float strenght)
    {
        if ((!_canJump && !_canDoubleJump) || _jumps >= 2)
            return;

        if (!IsJumping)
        {
            _jumps++;

            IsJumping = true;
            _speed.y = strenght;
        }
        else
        {
            _speed.y += strenght * Time.deltaTime;
        }
    }
}