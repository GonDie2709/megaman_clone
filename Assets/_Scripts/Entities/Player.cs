﻿using UnityEngine;

public class Player : BaseEntity
{
    private bool _jumpPressed = false;

	protected override void Update ()
    {
        base.Update();

        float horizontalAxis = Input.GetAxisRaw("Horizontal");

        if (horizontalAxis != 0)
            Move(Vector2.right * horizontalAxis);

        if (Input.GetButtonDown("Jump"))
        {
            if (!_jumpPressed)
            {
                if (!_canJump && _canDoubleJump)
                {
                    IsJumping = false;
                }

                _jumpPressed = true;
                Jump(_jumpStrenght);
            }
        }
        else if (Input.GetButtonUp("Jump"))
        {
            _jumpPressed = false;

            if (_canJump)
                _canJump = false;
        }

        if (IsJumping && Input.GetButton("Jump"))
            Jump(_jumpStrenght);
    }
}