﻿using System;

public class Enums
{
    [Flags]public enum Collisions { Top = 1, Bottom = 2, Right = 4, Left = 8, None = 0 }
    public enum ColliderBound { Top, TopLeft, TopRight, Right, RightTop, RightBottom, Bottom, BottomLeft, BottomRight, Left, LeftTop, LeftBottom, Center }
}