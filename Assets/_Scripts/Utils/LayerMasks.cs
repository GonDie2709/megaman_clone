﻿using UnityEngine;

public static class LayerMasks
{
    public static int Player = 1 << LayerMask.NameToLayer("Player");
}