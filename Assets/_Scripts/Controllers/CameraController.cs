﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Header("Scene Limits")]
    public Vector2 minScreenLimit;
    public Vector2 maxScreenLimit;

    [Header("Follow Config")]
    public Transform followtarget;
    public float followSpeed = 0.5f;

    [Header("Shake Config")]
    public float shakeFallout = 1f;

    Transform _transform;
    Camera _camera;

    Vector3 _shakeOffset;
    float _initialZ;

    bool _isShaking = false;
    float _shakeStrenght = 0f;
    float _shakeDuration = 0f;
    Vector3 _shakeOrigin;

    void Start ()
    {
        _transform = GetComponent<Transform>();
        _camera = GetComponent<Camera>();

        _initialZ = _transform.localPosition.z;
    }
	
	void Update ()
    {
        Vector3 heading = followtarget.localPosition - _transform.localPosition;
        float dist = heading.magnitude;

        Vector3 pos = _transform.localPosition;
        pos += heading * followSpeed * dist * Time.deltaTime;
        pos.z = _initialZ;

        if (pos.x <= minScreenLimit.x)
            pos.x = minScreenLimit.x;
        if (pos.x >= maxScreenLimit.x)
            pos.x = maxScreenLimit.x;

        if (pos.y <= minScreenLimit.y)
            pos.y = minScreenLimit.y;
        if (pos.y >= maxScreenLimit.y)
            pos.y = maxScreenLimit.y;

        if(_isShaking)
        {
            _shakeOffset = Random.insideUnitSphere * _shakeStrenght;

            _shakeDuration -= Time.deltaTime * shakeFallout;

            if (_shakeDuration <= 0f)
            {
                _isShaking = false;
                _shakeOffset = Vector3.zero;
                _transform.localPosition = _shakeOrigin;
            }
        }

        _transform.localPosition = pos + _shakeOffset;
    }

    public void ScreenShake(float strength, float duration)
    {
        _isShaking = true;
        _shakeStrenght = strength;
        _shakeDuration = duration;

        _shakeOrigin = _transform.localPosition;
    }
    
    public void ShakeStrenght(float strenght)
    {
        _shakeStrenght = strenght;
    }

    public void StopShake()
    {
        _shakeDuration = -1f;
    }
}