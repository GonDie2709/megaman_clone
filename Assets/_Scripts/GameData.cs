﻿using GonDie.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : Singleton<GameData>
{
    [Header("Physics")]
    public Vector2 gravity = new Vector2(0f, -0.5f);
    public Vector2 maxAcelSpeed = new Vector2(10f, 5f);
    public float physicsMaxRayDistance = 10f;
    public float limitJumpTime = 0.5f;
    public float groundFriction = 1f;
    public float airFriction = 0.5f;

    [Header("Development")]
    public bool debug;
}