﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;
using GonDie.Utils;
using GonDie.Utils.Events;

namespace GonDie.WebService
{
    public enum AccessTypes
    {
        State
    }

    public class WebService : Singleton<WebService>
    {
        public string url;
        public string token;
        public string projectId;
        public int errorIdCall = 100;

        //public string token { get; protected set; }

        public Dictionary<AccessTypes, string> accessPath;

        public delegate void WebObjectEvent(WebObject response);

        public event Events.StringEvent OnError;
        public event WebObjectEvent OnGet;

        protected override void OnAwake()
        {
/*#if !UNITY_EDITOR
        dataFolder = Application.persistentDataPath + "/" + dataFolder;
#endif*/

            accessPath = new Dictionary<AccessTypes, string>()
            {
                {AccessTypes.State, "states"}
            };
        }

        public void GetJson(AccessTypes accessTypes, string parameter, WebObjectEvent returnStringEvent)
        {
            var path = url + "/" + accessPath[accessTypes] + parameter;

            StartCoroutine(_GetJson(accessTypes, path, returnStringEvent));
        }
        public void GetJson(AccessTypes accessTypes, WebObjectEvent returnStringEvent)
        {
            var path = url + "/" + accessPath[accessTypes];

            StartCoroutine(_GetJson(accessTypes, path, returnStringEvent));
        }
        public IEnumerator _GetJson(AccessTypes accessTypes, string path, WebObjectEvent returnStringEvent)
        {
            WWW connection = new WWW(path);

            yield return connection;

            if (string.IsNullOrEmpty(connection.error))
            {
                Debug.Log("Received: " + connection.text);

                WebObject wObj = new WebObject();
                wObj.rawJson = connection.text;

                returnStringEvent(wObj);
            }
            else
            {
                Debug.LogError("Error: " + connection.error);

                if (OnError != null) OnError(connection.error);

                returnStringEvent(null);
                yield break;
            }
        }
        public void PostJson(AccessTypes accessTypes, string json, WebObjectEvent Callback)
        {
            StartCoroutine(_PostJson(accessTypes, json, Callback));
        }
        protected IEnumerator _PostJson(AccessTypes accessTypes, string json, WebObjectEvent Callback)
        {
            var path = url + "/" + accessPath[accessTypes];
            var connection = CreatePostConnection(path, json);

            yield return connection.SendWebRequest();
            SendFeedback(connection, Callback);
        }

        void SendFeedback(UnityWebRequest connection, WebObjectEvent Callback)
        {
            var responseCode = (int)connection.responseCode;
            if (connection.isNetworkError)
            {
                Debug.LogError(connection.error);
                //FeedbackServerError(connection.downloadHandler, responseCode);
                //FeedbackServerError(connection.error, responseCode);
                return;
            }

            var webres = new WebObject();
            webres.rawJson = connection.downloadHandler.text;

            Callback(webres);
            if (OnGet != null) OnGet(webres);
        }

        UnityWebRequest CreatePostConnection(string path, string json)
        {
            var connection = new UnityEngine.Networking.UnityWebRequest(path, UnityWebRequest.kHttpVerbPOST);
            byte[] bytes = Encoding.UTF8.GetBytes(json);
            var uH = new UploadHandlerRaw(bytes);
            var dH = new DownloadHandlerBuffer();
            uH.contentType = "application/json";
            connection.uploadHandler = uH;
            connection.downloadHandler = dH;

            return connection;
        }

        public string GetUrl()
        {
            //TODO: salvar no playerprefs
            return url;
        }

        public void SetUrl(string value)
        {
            url = value;
        }
    }

    public class WebUsers
    {
        public List<WebUser> entities;
    }

    public class WebUser
    {
        public string id;
        public string name;
        public string gd;
        public string tipo;
    }
    public class WebErr
    {
        public string error;
        public int status;
        public string summary;
    }
    public class WebObject
    {
        public string rawJson;
        public WebErr err;
        public List<WebUser> list;
    }

    public class WebScore
    {
        public string key;
        public string project;
        //public List<User> entities;
    }
}