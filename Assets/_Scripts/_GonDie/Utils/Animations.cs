﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GonDie.Utils.Events;

namespace GonDie.Utils
{
    public static class Animations
    {
        #region Transform
        public static void LocalRotate(this Transform transform, MonoBehaviour caller, Quaternion toValue, float duration, float delay, EaseType ease = EaseType.Linear, Events.Events.SimpleEvent OnStart = null, Events.Events.SimpleEvent OnEnd = null)
        {
            caller.StartCoroutine(_LocalRotate(transform, toValue, duration, delay, ease, OnStart, OnEnd));
        }
        static IEnumerator _LocalRotate(Transform transform, Quaternion toValue, float duration, float delay, EaseType ease = EaseType.Linear, Events.Events.SimpleEvent OnStart = null, Events.Events.SimpleEvent OnEnd = null)
        {
            float time = 0f;
            Quaternion fromValue = transform.localRotation;

            if (OnStart != null)
                OnStart();

            yield return new WaitForSeconds(delay);

            while (time <= 1f)
            {
                time += Time.deltaTime / duration;

                transform.localRotation = Quaternion.Lerp(fromValue, toValue, Easing.Ease(ease, time));

                yield return null;
            }

            if (OnEnd != null)
                OnEnd();
        }
        #endregion

        #region RectTransform
        public static void LerpSize(this RectTransform rectTransform, MonoBehaviour caller, Vector2 toSize, float duration, float delay, EaseType ease = EaseType.Linear, Events.Events.SimpleEvent OnStart = null, Events.Events.SimpleEvent OnEnd = null)
        {
            caller.StartCoroutine(_LerpSize(rectTransform, toSize, duration, delay, ease, OnStart, OnEnd));
        }
        static IEnumerator _LerpSize(RectTransform rectTransform, Vector2 toSize, float duration, float delay, EaseType ease, Events.Events.SimpleEvent OnStart, Events.Events.SimpleEvent OnEnd)
        {
            float time = 0f;
            Vector2 fromSize = rectTransform.sizeDelta;

            if (OnStart != null)
                OnStart();

            yield return new WaitForSeconds(delay);

            while(time <= 1f)
            {
                time += Time.deltaTime / duration;

                rectTransform.sizeDelta = Vector2.Lerp(fromSize, toSize, Easing.Ease(ease, time));

                yield return null;
            }

            if (OnEnd != null)
                OnEnd();
        }

        public static void LerpAnchor(this RectTransform rectTransform, MonoBehaviour caller, Vector2 toAnchor, float duration, float delay, EaseType ease = EaseType.Linear, Events.Events.SimpleEvent OnStart = null, Events.Events.SimpleEvent OnEnd = null)
        {
            caller.StartCoroutine(_LerpAnchor(rectTransform, toAnchor, duration, delay, ease, OnStart, OnEnd));
        }
        static IEnumerator _LerpAnchor(RectTransform rectTransform, Vector2 toAnchor, float duration, float delay, EaseType ease, Events.Events.SimpleEvent OnStart, Events.Events.SimpleEvent OnEnd)
        {
            float time = 0f;
            Vector2 fromAnchor = rectTransform.anchoredPosition;

            if (OnStart != null)
                OnStart();

            yield return new WaitForSeconds(delay);

            while (time <= 1f)
            {
                time += Time.deltaTime / duration;

                rectTransform.anchoredPosition = Vector2.Lerp(fromAnchor, toAnchor, Easing.Ease(ease, time));

                yield return null;
            }

            if (OnEnd != null)
                OnEnd();
        }
        #endregion

        #region Materials

        public static void LerpColor(this Material mat, MonoBehaviour caller, float toValue, float duration, float delay, EaseType ease = EaseType.Linear, Events.Events.SimpleEvent OnStart = null, Events.Events.SimpleEvent OnEnd = null)
        {
            caller.StartCoroutine(_LerpColor(mat, toValue, duration, delay, ease, OnStart, OnEnd));
        }
        static IEnumerator _LerpColor(Material mat, float toValue, float duration, float delay, EaseType ease, Events.Events.SimpleEvent OnStart, Events.Events.SimpleEvent OnEnd)
        {
            float time = 0f;
            Color fromColor = mat.color;
            Color toColor = fromColor;
            toColor.a = toValue;

            if (OnStart != null)
                OnStart();

            yield return new WaitForSeconds(delay);

            while (time <= 1f)
            {
                time += Time.deltaTime / duration;

                mat.color = Color.Lerp(fromColor, toColor, Easing.Ease(ease, time));

                yield return null;
            }

            if (OnEnd != null)
                OnEnd();
        }
        #endregion

        #region CanvasGroup
        public static void Fade(this CanvasGroup cg, MonoBehaviour caller, float toValue, float duration, float delay, EaseType ease = EaseType.Linear, Events.Events.SimpleEvent OnStart = null, Events.Events.SimpleEvent OnEnd = null)
        {
            caller.StartCoroutine(_Fade(cg, toValue, duration, delay, ease, OnStart, OnEnd));
        }
        static IEnumerator _Fade(CanvasGroup cg, float toValue, float duration, float delay, EaseType ease, Events.Events.SimpleEvent OnStart = null, Events.Events.SimpleEvent OnEnd = null)
        {
            float time = 0f;
            float fromValue = cg.alpha;

            if (OnStart != null)
                OnStart();

            yield return new WaitForSeconds(delay);

            while(time <= 1f)
            {
                time += Time.deltaTime / duration;

                cg.alpha = Mathf.Lerp(fromValue, toValue, Easing.Ease(ease, time));

                yield return null;
            }

            if (OnEnd != null)
                OnEnd();
        }
        #endregion
    }
}