﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GonDie.Utils.Events
{
    public class Events
    {
        public delegate void SimpleEvent();
        public delegate void IntEvent(int i);
        public delegate void IntSimpleEvent(int i, SimpleEvent ev);
        public delegate void FloatEvent(float f);
        public delegate void StringEvent(string s);
        public delegate void BoolEvent(bool b);
        public delegate void ObjectEvent(object arg);
        
    }
}