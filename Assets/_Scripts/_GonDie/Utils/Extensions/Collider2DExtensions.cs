﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GonDie.Utils.Extensions
{
    public static class Collider2DExtensions
    {
        public static Vector2 GetBound(this Collider2D collider2D, Enums.ColliderBound bound)
        {
            switch(bound)
            {
                case Enums.ColliderBound.Top:
                    return collider2D.bounds.center + Vector3.up * collider2D.bounds.extents.y;
                case Enums.ColliderBound.TopLeft:
                case Enums.ColliderBound.LeftTop:
                    return collider2D.bounds.center + new Vector3(-collider2D.bounds.extents.x, collider2D.bounds.extents.y, 0f);
                case Enums.ColliderBound.TopRight:
                case Enums.ColliderBound.RightTop:
                    return collider2D.bounds.center + new Vector3(collider2D.bounds.extents.x, collider2D.bounds.extents.y, 0f);
                case Enums.ColliderBound.Bottom:
                    return collider2D.bounds.center - Vector3.up * collider2D.bounds.extents.y;
                case Enums.ColliderBound.BottomLeft:
                case Enums.ColliderBound.LeftBottom:
                    return collider2D.bounds.center + new Vector3(-collider2D.bounds.extents.x, -collider2D.bounds.extents.y, 0f);
                case Enums.ColliderBound.BottomRight:
                case Enums.ColliderBound.RightBottom:
                    return collider2D.bounds.center + new Vector3(collider2D.bounds.extents.x, -collider2D.bounds.extents.y, 0f);
                case Enums.ColliderBound.Left:
                    return collider2D.bounds.center - Vector3.right * collider2D.bounds.extents.x;
                case Enums.ColliderBound.Right:
                    return collider2D.bounds.center + Vector3.right * collider2D.bounds.extents.x;
                case Enums.ColliderBound.Center:
                default:
                    return collider2D.bounds.center;
            }
        }
    }
}