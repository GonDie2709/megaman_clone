﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class BaseButton : MonoBehaviour
{
    protected Button _button;
    AudioSource _audioSrc;

	void Awake ()
    {
        _audioSrc = GetComponent<AudioSource>();
        _button = GetComponent<Button>();
        _button.onClick.AddListener(() =>
        {
            OnClick();
        });
    }

    private void Start()
    {
        OnStart();
    }

    protected virtual void OnStart()
    {

    }

    protected virtual void OnClick()
    {
        _audioSrc.Play();
    }
}
