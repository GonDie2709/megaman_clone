﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace GonDie.Pagination
{
    public class ButtonPageClose : BaseButton
    {
        public int pageIndex;

        protected override void OnClick()
        {
            base.OnClick();

            PageManager.Instance.ClosePage(pageIndex);
        }
    }
}