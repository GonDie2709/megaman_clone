﻿using GonDie.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GonDie.Utils.Events;
using UnityEngine.UI;

namespace GonDie.Pagination
{
    public class PageManager : Singleton<PageManager>
    {
        public event Events.IntEvent OnOpenPage, OnClosePage;
        public event Events.IntSimpleEvent OnRegisterPageOpenEvent, OnRegisterPageCloseEvent;

        int _currentPage = 0;
        public int currentPage
        {
            get { return _currentPage; }
            private set { _currentPage = value; }
        }

        private void Start()
        {
            StartCoroutine(_Start());
        }

        IEnumerator _Start()
        {
            yield return new WaitForSeconds(1f);

            OpenPage(0);
        }

        public void OpenPage(int index, float delay = 0f)
        {
            if (currentPage == index)
                return;

            StartCoroutine(_OpenPage(index, delay));
        }
        IEnumerator _OpenPage(int index, float delay)
        {
            yield return new WaitForSeconds(delay);

            if (OnOpenPage != null)
                OnOpenPage(index);

            currentPage = index;
        }

        public void ClosePage(int index)
        {
            if (OnClosePage != null)
                OnClosePage(index);
        }

        public void RegisterPageOpenEvent(int index, Events.SimpleEvent ev)
        {
            if (OnRegisterPageOpenEvent != null)
                OnRegisterPageOpenEvent(index, ev);
        }

        public void RegisterPageCloseEvent(int index, Events.SimpleEvent ev)
        {
            if (OnRegisterPageCloseEvent != null)
                OnRegisterPageCloseEvent(index, ev);
        }
    }
}