﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GonDie.Utils.Events;
using GonDie.Utils;

namespace GonDie.Pagination
{
    [RequireComponent(typeof(CanvasGroup))]
    public class Page : MonoBehaviour
    {
        public int pageIndex;

        RectTransform _rTrans;
        CanvasGroup _cv;

        public event Events.SimpleEvent OnPageOpen, OnPageClose;

        void Start()
        {
            _rTrans = GetComponent<RectTransform>();
            _cv = GetComponent<CanvasGroup>();

            PageManager.Instance.OnOpenPage += Open;
            PageManager.Instance.OnClosePage += Close;

            PageManager.Instance.OnRegisterPageOpenEvent += (int index, Events.SimpleEvent ev) =>
            {
                if (index == pageIndex)
                    OnPageOpen += ev;
            };

            PageManager.Instance.OnRegisterPageCloseEvent += (int index, Events.SimpleEvent ev) =>
            {
                if (index == pageIndex)
                    OnPageClose += ev;
            };
        }

        void Open(int i)
        {
            if (pageIndex == i)
            {
                

                _cv.Fade(this, 1f, 1f, 0f, EaseType.CubicInOut, () =>
                {
                    _cv.interactable = true;
                    _cv.blocksRaycasts = true;
                },
                () =>
                {
                    if (OnPageOpen != null)
                        OnPageOpen();
                });
            }
            else
                Close(pageIndex);
        }

        void Close(int i)
        {
            if (pageIndex != i) return;

            if (OnPageClose != null)
                OnPageClose();

            _cv.Fade(this, 0f, 1f, 0f, EaseType.CubicInOut, () =>
            {
                _cv.interactable = false;
                _cv.blocksRaycasts = false;
            });
        }
    }
}