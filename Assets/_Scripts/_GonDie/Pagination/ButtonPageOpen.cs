﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace GonDie.Pagination
{
    public class ButtonPageOpen : BaseButton
    {
        public int pageIndex;
        public float delay = 0f;

        protected override void OnClick()
        {
            base.OnClick();

            PageManager.Instance.OpenPage(pageIndex, delay);
        }
    }
}