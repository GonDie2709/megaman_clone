﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTest : MonoBehaviour
{
    public float movSpd = 5f;
    public float jumpStr = 5f;
    public float gravityForce = 2f;

    Vector2 move;
    float horizontalMove, verticalMove;
    float jumpTime;
    bool isJumping;

    Transform _transform;
    CharacterController _charController;
    Animator _animator;

	void Start ()
    {
        _transform = GetComponent<Transform>();
        _charController = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        if (isJumping)
        {
            jumpTime += Time.fixedDeltaTime;

            if (jumpTime >= 0.5f)
                isJumping = false;
        }

        if (!_charController.isGrounded && !isJumping)
        {
            _animator.SetBool("isJumping", false);
            verticalMove += Physics2D.gravity.y * Time.deltaTime;
        }
        else if (_charController.isGrounded && !isJumping)
        {
            _animator.SetBool("hasGrounded", true);

            isJumping = false;
            verticalMove = 0f;
            jumpTime = 0f;
        }

        verticalMove = Mathf.Clamp(verticalMove, Physics2D.gravity.y, 1000f);
        move.x = horizontalMove;
        move.y = verticalMove;

        _charController.Move(move * Time.deltaTime);
    }

    void Update ()
    {
        _animator.SetBool("isRunning", false);

        horizontalMove = 0f;

        float mov = Input.GetAxisRaw("Horizontal");
        if(mov != 0)
        {
            _animator.SetBool("isRunning", true);
            _transform.localScale = new Vector3(mov, 1f, 1f);
            horizontalMove = movSpd * mov;
            //_charController.Move(Vector3.right * movSpd * Time.deltaTime * mov);
            //_transform.position += Vector3.right * movSpd * Time.deltaTime * mov;
        }

        if(_charController.isGrounded && !isJumping)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _animator.SetBool("isJumping", true);
                _animator.SetBool("hasGrounded", false);

                isJumping = true;
                verticalMove += jumpStr;
            }
        }
        else if(isJumping && Input.GetKey(KeyCode.Space))
        {
            verticalMove += jumpStr * (1f * jumpTime / 1.25f) * Time.deltaTime;
        }
        else if(isJumping && Input.GetKeyUp(KeyCode.Space))
        {
            isJumping = false;
        }
        
        ///////////////////////////////////////////
        
    }
}
